from django import forms
from django.core.validators import RegexValidator

# Contact Form on main
class ContactForm(forms.Form):
    name = forms.CharField(max_length=100, help_text='Your name')
    email = forms.EmailField(help_text='Valid Email address please')
    phone = forms.CharField(max_length=50, help_text='Your phone')
    subject = forms.CharField(max_length=100)
    message = forms.CharField(widget=forms.Textarea, help_text='What do you want to say?')

# Credit Application Form
class CreditApplicationForm(forms.Form):

    ########    Personal info     ########

    first_name = forms.CharField(max_length=100, help_text='Your name')
    mid_name = forms.CharField(max_length=100, help_text='Your name')
    last_name = forms.CharField(max_length=100, help_text='Your name')
    email = forms.EmailField(help_text='Valid Email address please')
    social_security = forms.CharField(max_length=100, help_text='Your name')
    birth_date = forms.DateField(help_text='Your birthday date', input_formats=['%b/%d/%Y'])
    home_phone = forms.CharField(
        max_length=50,
        help_text='Your home phone',
        error_messages={'incomplete': 'Enter a phone number.'},
        validators=[RegexValidator(r'^[0-9]+$', 'Enter a valid phone number.')]
    )
    mobile_phone = forms.CharField(
        max_length=50,
        help_text='Your mobile phone',
        error_messages = {'incomplete': 'Enter a phone number.'},
        validators = [RegexValidator(r'^[0-9]+$', 'Enter a valid phone number.')]
    )
    address = forms.CharField(max_length=100, help_text='Your address')
    city = forms.CharField(max_length=100)
    state = forms.CharField(max_length=100)
    zip = forms.CharField(
        max_length=5,
        error_messages={'incomplete': 'Enter a phone number.'},
        validators=[RegexValidator(r'^[0-9]+$', 'Enter a valid phone number.')]
    )
    c = [("1", "Own"), ("2", "Rent"), ("3", "Finance")]
    occ_type = forms.ChoiceField(label="Choices", choices = c)
    other_occ_type = forms.CharField(max_length=100)
    res_type = forms.CharField(max_length=100)
    rent_payment = forms.CharField(max_length=100)
    time_at_res = forms.CharField(max_length=100)
    acc_in_your_name = forms.CharField(max_length=100)
    driver_lic_number = forms.CharField(max_length=100)
    driver_lic_state = forms.CharField(max_length=100)
    driver_lic_expdate = forms.DateField(input_formats=['%b/%d/%Y'])

    ########    Employment info     ########

    empl_name = forms.CharField(max_length=100, help_text='Your name')
    empl_address = forms.CharField(max_length=100, help_text='Your address')
    empl_phone = forms.CharField(
        max_length=50,
        help_text='Your home phone',
        error_messages={'incomplete': 'Enter a phone number.'},
        validators=[RegexValidator(r'^[0-9]+$', 'Enter a valid phone number.')]
    )
    empl_city = forms.CharField(max_length=100)
    empl_state = forms.CharField(max_length=100)
    empl_zip = forms.CharField(
        max_length=5,
        error_messages={'incomplete': 'Enter a phone number.'},
        validators=[RegexValidator(r'^[0-9]+$', 'Enter a valid phone number.')]
    )
    empl_occupation = forms.CharField(max_length=100)
    empl_time_on_job = forms.CharField(max_length=100)
    empl_salary = forms.CharField(max_length=100)
    empl_other_income = forms.CharField(max_length=100)
    #checkbox for AGREE !!!!

    ########    Vehicle information     ########
    vehicle_make = forms.CharField(max_length=100)
    vehicle_model = forms.CharField(max_length=100)
    vehicle_year = forms.CharField(max_length=100)
    vehicle_mileage = forms.CharField(max_length=100)
    vehicle_salesman_name = forms.CharField(max_length=100)




