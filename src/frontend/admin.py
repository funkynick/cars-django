from django.contrib import admin
from frontend.models import Car

# Register your models here.
class CarAdmin(admin.ModelAdmin):
    pass
    list_display = ('brand','model', 'year', 'price')
    search_fields = ('brand','model')

admin.site.register(Car, CarAdmin)
