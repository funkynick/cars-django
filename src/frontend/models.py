from django.db import models

# Create your models here.
class Car(models.Model):
    brand = models.CharField(max_length=200)
    model = models.CharField(max_length=200)
    year = models.PositiveSmallIntegerField()
    lease_term = models.CharField(max_length=200)
    mile = models.CharField(max_length=200)
    msrp = models.IntegerField()
    price = models.IntegerField()

    # Photo of the car
    photo = models.ImageField(default='default.png', upload_to='car_photo')

    # If selected it will be special price filter
    deal = models.BooleanField("Deal of the month", default=False)

    #Filter for car body type
    sedan = 'sedan'
    coupe = 'coupe'
    SUV = 'cuv'
    hatchback = 'hatchback'
    body_choices = (
        (sedan, 'Sedan'),
        (coupe, 'Coupe'),
        (SUV, 'SUV'),
        (hatchback, 'Hatchback')
    )
    body = models.CharField(
        max_length=100,
        choices=body_choices,
        default=sedan,
    )

    @property
    def msrpValue(self):
        return "%.3f" % self.msrp

    #admin page cars name showing
    def __str__(self):
        return self.model
