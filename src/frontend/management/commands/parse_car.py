from django.core.management.base import BaseCommand
from requests_html import HTMLSession
from datetime import datetime

from frontend.models import Car


class Command(BaseCommand):

    # page with all urls
    CATALOG_URL = 'https://viplease.com/vehicle-listings/?view_type=grid&per_page=-1&order=_auto_price-asc'

    def handle(self,**options):

        self.session = HTMLSession()
        #self.parseCatalog()
        self.parseCatalogFromFile()

    # parsing page with urls and write it to file
    def parseCatalog(self):

        response = self.session.get(self.CATALOG_URL)
        all_links = response.html.links

        # first variant of "for" loop
        car_links = []

        with open('all_urls.txt', 'w') as file:
            for link in all_links:
                if ('autos' in link):
                    car_links.append(link)
                    file.write("%s\n" % link)

    # parse urls from file and save it to model properties in database
    def parseCatalogFromFile(self):

        #with open('test_urls.txt', 'r') as file:
        with open('all_urls.txt', 'r') as file:
            items = file.readlines()


        # second variant of "for" loop
        items = [item.strip("\n") for item in items]

        for item in items:
            print("Parsing %s" % item)
            single_car_info = self.parseSingleCarInfo(item)

            new_car = Car()
            new_car.year = single_car_info['year']
            new_car.brand = single_car_info['brand'].capitalize()
            new_car.model = single_car_info['model'].capitalize()
            new_car.lease_term = single_car_info['lease_term']
            new_car.mile = str(single_car_info['mile'])
            new_car.msrp = single_car_info['msrp']
            new_car.price = single_car_info['price']
            new_car.save()

            print("Car %s %s is saved into database" % (new_car.brand, new_car.model))

    # parse info from single url and filter it for saving in database
    def parseSingleCarInfo(self, url):

        response = self.session.get(url)

        carinfo={}

        #get info from div with info about car

        info = response.html.find('#tab1', first=True)
        items = info.text.lower().split("\n")
        now = datetime.now()

        #set string with car parameters
        try:
            carinfo['year'] = int([s for s in items if "year" in s][0].split(":")[1].strip("\xa0").replace(" ", ""))
        except ValueError:
            carinfo['year'] = int(now.strftime("%Y"))

        try:
            carinfo['brand'] = [s for s in items if "make" in s][0].split(":")[1].strip("\xa0").replace(" ", "")
        except IndexError:
            if "volkswagen" in url:
                carinfo['brand'] = "Volkswagen"
            if "dodge" in url:
                carinfo['brand'] = "Dodge"
            if "jeep" in url:
                carinfo['brand'] = "Jeep"
            if "jaguar" in url:
                carinfo['brand'] = "Jaguar"
            if "chevy" in url:
                carinfo['brand'] = "Chevrolet"
            if "chrysler" in url:
                carinfo['brand'] = "Chrysler"
            if "gmc" in url:
                carinfo['brand'] = "GMC"
            if "cadillac" in url:
                carinfo['brand'] = "Cadillac"
            if "mercedes" in url:
                carinfo['brand'] = "Mercedes Benz"
            if "honda" in url:
                carinfo['brand'] = "Honda"
            if "hyundai" in url:
                carinfo['brand'] = "Hyundai"
            if "toyota" in url:
                carinfo['brand'] = "Toyota"
            if "lexus" in url:
                carinfo['brand'] = "Lexus"
            if "land" in url:
                carinfo['brand'] = "Land Rover"
            if "nissan" in url:
                carinfo['brand'] = "Nissan"
            if "mazda" in url:
                carinfo['brand'] = "Mazda"
            if "subaru" in url:
                carinfo['brand'] = "Subaru"
            if "acura" in url:
                carinfo['brand'] = "Acura"
            if "maserati" in url:
                carinfo['brand'] = "Maseratti"
            if "porsche" in url:
                carinfo['brand'] = "Porsche"
            if "ram" in url:
                carinfo['brand'] = "RAM"
            if "alfa" in url:
                carinfo['brand'] = "Alfa Romeo"
            if "bentley" in url:
                carinfo['brand'] = "Bentley"

        try:
            carinfo['model'] = [s for s in items if "model" in s][0].split(":")[1].strip("\xa0")
        except IndexError:
            if "rx350" in url:
                carinfo['model'] = "RX350"
            if "ux200" in url:
                carinfo['model'] = "UX200"
            if "es350" in url:
                carinfo['model'] = "ES350"
            if "rx450h" in url:
                carinfo['model'] = "RX450H"
            if "cayenne" in url:
                carinfo['model'] = "Cayenne"
            if "giula" in url:
                carinfo['model'] = "Giula"
            if "mdx" in url:
                carinfo['model'] = "MDX"
            if "sierra" in url:
                carinfo['model'] = "Sierra"


        try:
            carinfo['lease_term'] = [s for s in items if "lease" in s][0].split(":")[1].strip("\xa0")
        except IndexError:
            carinfo['lease_term'] = "36 months"
        carinfo['mile'] = [s for s in items if "mile" in s][0].split(":")[1].strip("\xa0").replace(" ", "")
        try:
            carinfo['msrp'] = float([s for s in items if "msrp" in s][0].split(":")[1].strip("\xa0").replace(",", ".").replace(" ", "").replace("$", "").replace("months", "").replace("Down!", "").replace("/year", ""))
        except IndexError:
            carinfo['msrp'] = 1.00
            print("This car has no msrp price")
        except ValueError:
            carinfo['msrp'] = 1.00
            print("This car has no msrp price")

        try:
            carinfo['price'] = float([s for s in items if "price" in s][0].split(":")[1].strip("$\xa0/month lease payment (with loyalty)").replace(",", "."))
        except IndexError:
            carinfo['price'] = 1
            print("This car has no price")
        except ValueError:
            carinfo['price'] = 1
            print("This car has no price")

        return carinfo


