import datetime
import json

from django.views.generic import TemplateView, View
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from frontend.models import Car

from django.conf import settings
from django.core.mail import send_mail, EmailMessage, EmailMultiAlternatives
from .forms import ContactForm, CreditApplicationForm
from django.template.loader import render_to_string
from .render import Render


class MainPage(TemplateView):
    template_name='cars.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cars'] = Car.objects.order_by('brand')
        context['isMainPage'] = True
        context['contact_form'] = ContactForm()

        return context

    #simple email send from contact form

    def post(self, request, *args, **kwargs):

        contact_form = ContactForm(request.POST)

        if contact_form.is_valid():

            name = contact_form.cleaned_data['name']
            subject = contact_form.cleaned_data['subject']
            message = contact_form.cleaned_data['message']
            from_email = contact_form.cleaned_data['email']

            html_content = render_to_string ('email_template.html', {'contact_form': contact_form} )
            to = ['nkokov4@gmail.com']

            msg = EmailMultiAlternatives(subject, html_content, from_email, to)
            msg.attach_alternative(html_content, "text/html")
            msg.send()

            resp = {}
            resp['message'] = 'Message was sent!'
            return JsonResponse(resp)


        return render(request, self.template_name, {'contact_form': contact_form})


class SingleCarPage(TemplateView):
    template_name='single_car.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['car'] = Car.objects.get(pk=self.kwargs['pk'])
        context['isMainPage'] = False
        return context

class CreditApp(TemplateView):
    template_name='credit_app.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['isMainPage'] = False
        context['creditapp_form'] = CreditApplicationForm()

        #print(self.kwargs)

        return context

    #send credit application form on email

    def post(self, request, *args, **kwargs):

        creditapp_form = CreditApplicationForm(request.POST)
        context = {}
        context['creditapp_form'] = creditapp_form


        if creditapp_form.is_valid():

            #Personal info

            first_name = creditapp_form.cleaned_data['first_name']
            mid_name = creditapp_form.cleaned_data['mid_name']
            last_name = creditapp_form.cleaned_data['last_name']
            email = creditapp_form.cleaned_data['email']
            social_security = creditapp_form.cleaned_data['social_security']
            birth_date = creditapp_form.cleaned_data['birth_date']
            home_phone = creditapp_form.cleaned_data['home_phone']
            mobile_phone = creditapp_form.cleaned_data['mobile_phone']
            address = creditapp_form.cleaned_data['address']
            city = creditapp_form.cleaned_data['city']
            state = creditapp_form.cleaned_data['state']
            zip = creditapp_form.cleaned_data['zip']
            occ_type = creditapp_form.cleaned_data['occ_type']
            other_occ_type = creditapp_form.cleaned_data['other_occ_type']
            res_type = creditapp_form.cleaned_data['res_type']
            rent_payment = creditapp_form.cleaned_data['rent_payment']
            time_at_res = creditapp_form.cleaned_data['time_at_res']
            acc_in_your_name = creditapp_form.cleaned_data['acc_in_your_name']
            driver_lic_number = creditapp_form.cleaned_data['driver_lic_number']
            driver_lic_state = creditapp_form.cleaned_data['driver_lic_state']
            driver_lic_expdate = creditapp_form.cleaned_data['driver_lic_expdate']

            #Employment info

            empl_name = creditapp_form.cleaned_data['empl_name']
            empl_address = creditapp_form.cleaned_data['empl_address']
            empl_phone = creditapp_form.cleaned_data['empl_phone']
            empl_city = creditapp_form.cleaned_data['empl_city']
            empl_state = creditapp_form.cleaned_data['empl_state']
            empl_zip = creditapp_form.cleaned_data['empl_zip']
            empl_occupation = creditapp_form.cleaned_data['empl_occupation']
            empl_time_on_job = creditapp_form.cleaned_data['empl_time_on_job']
            empl_salary = creditapp_form.cleaned_data['empl_salary']
            empl_other_income = creditapp_form.cleaned_data['empl_other_income']

            # Vehicle information

            vehicle_make = creditapp_form.cleaned_data['vehicle_make']
            vehicle_model = creditapp_form.cleaned_data['vehicle_model']
            vehicle_year = creditapp_form.cleaned_data['vehicle_year']
            vehicle_mileage = creditapp_form.cleaned_data['vehicle_mileage']
            vehicle_salesman_name = creditapp_form.cleaned_data['vehicle_salesman_name']

            current_date = datetime.datetime.now()
            context['current_date'] = current_date

            #render HTML page
            html_content = render_to_string('creditapp_email_template.html', context)

            #render PDF
            pdf_content = Render.render_to_file('creditapp_pdf_template.html', context)
            file_path = pdf_content[1]
            file_name = pdf_content[0]

            #to = ['nkokov4@gmail.com']
            to = ['akirmaz@gmail.com']

            subject = 'credit application'
            from_email = 'hello@autoleasingflorida.com'

            msg = EmailMultiAlternatives(subject, html_content, from_email, to)
            msg.attach_alternative(html_content, "text/html")
            with open(file_path, "rb") as f:
                file_contents = f.read()
            msg.attach(file_name, file_contents, "application/pdf")
            msg.send()

            resp = {}
            resp['message'] = 'Credit application was sent!'
            return JsonResponse(resp)

        # else:
        #     return HttpResponse('Form is invalid!')

        return render(request, self.template_name, context)


class Success(TemplateView):
    template_name='success.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['isMainPage'] = False
        print(self.kwargs)
        return context

class FaqPage(TemplateView):
    template_name='faq.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['isMainPage'] = False
        print(self.kwargs)
        return context

# Contact Form on mainpage


